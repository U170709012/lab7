public class MyDate {

    private int year;
    private int day;
    private int month;
    int[] maxDays = { 31, 28, 31, 30, 31, 30, 31, 30, 31, 31, 30, 31 };

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
        fixIfLeapYear();
    }

    public void decrementDay() {
        decrementDay(1);
    }

    public void decrementDay(int day) {
        this.day -= day;
        while (0 >= this.day) {
            decrementMonthP(1);
            this.day += maxDays[this.month - 1];
        }
    }

    public void decrementMonth() {
        decrementMonth(1);
    }

    public void decrementMonth(int month) {
        int prevMaxDay = this.maxDays[this.month - 1];
        this.month -= month;
        while (0 >= this.month) {
            this.month += 12;
            decrementYear();
        }
        if (this.day == prevMaxDay && this.day > this.maxDays[this.month - 1]) {
            this.day = this.maxDays[this.month - 1];
        }
    }

    private void decrementMonthP(int month) {
        this.month -= month;
        while (0 >= this.month) {
            this.month += 12;
            decrementYear();
            System.out.println("Decrementh Month");
        }
    }

    public void incrementMonthP(int month) {
        this.month += month;
        while (this.month > 12) {
            this.month -= 12;
            incrementYear();
        }
    }

    public void incrementDay() {
        incrementDay(1);
    }

    public void incrementDay(int day) {
        this.day += day;
        while (maxDays[this.month - 1] < this.day) {
            this.day -= maxDays[this.month - 1];
            incrementMonthP(1);
        }
    }

    public void incrementMonth() {
        incrementMonth(1);
    }

    public void incrementMonth(int month) {
        int prevMaxDay = this.maxDays[this.month - 1];
        this.month += month;
        while (this.month > 12) {
            this.month -= 12;
            incrementYear();
        }
        if (this.day == prevMaxDay && this.day > this.maxDays[this.month - 1]) {
            this.day = this.maxDays[this.month - 1];
        }
    }

    public void incrementYear() {
        incrementYear(1);
    }

    public void incrementYear(int year) {
        this.year += year;
        fixIfLeapYear();
    }

    public void decrementYear() {
        decrementYear(1);
    }

    public void decrementYear(int year) {
        this.year -= year;
        fixIfLeapYear();
    }

    private void fixIfLeapYear() {
        if (isLeapYear()) {
            maxDays[1] = 29;
        } else {
            maxDays[1] = 28;
        }
        if (this.month == 2 && (this.day == 29 || this.day == 28)) {
            this.day = this.maxDays[this.month - 1];
        }
    }

    private boolean isLeapYear() {
        if ((2024 - this.year) % 4 == 0)
            return true;
        return false;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public int getDay() {
        return day;
    }

    public String toString() {
        return "" + year + "-" + (month > 9 ? "" : "0") + (month) + "-" + (day > 9 ? "" : "0") + day;
    }

    public boolean isBefore(MyDate another) {
        if (this.year < another.getYear()) {
            return true;
        } else if (this.year == another.getYear()) {
            if (this.month < another.getMonth()) {
                return true;
            } else if (this.month == another.getMonth()) {
                if (this.day < another.getDay())
                    return true;
                else
                    return false;
            }
            return false;
        }
        return false;
    }

    public boolean isAfter(MyDate another) {
        if (this.year > another.getYear()) {
            return true;
        } else if (this.year == another.getYear()) {
            if (this.month > another.getMonth()) {
                return true;
            } else if (this.month == another.getMonth()) {
                if (this.day > another.getDay())
                    return true;
                else
                    return false;
            }
            return false;
        }
        return false;
    }

    public int dayDifference(MyDate anotherDate) {
        // TODO Auto-generated method stub
        int total = 0;
        if (year == anotherDate.getYear()) {
            if (month > anotherDate.getMonth()) {
                total = maxDays[anotherDate.getMonth() - 1];
                total = total + (maxDays[month - 1] - day);
                return total - 1;
            }

        }
        return (total - 1);

    }

}